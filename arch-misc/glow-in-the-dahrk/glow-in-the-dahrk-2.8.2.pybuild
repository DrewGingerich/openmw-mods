# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Glow in the Dahrk"
    DESC = "Windows transition to glowing versions at night"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45886"
    # From README (paraphrased): Assets may be used, but you may not rehost this
    # unless I have been unable to be reached for 3 months.
    LICENSE = "free-derivation"
    RDEPEND = """
        base/morrowind
        tr? ( landmasses/tamriel-rebuilt )
    """
    KEYWORDS = "openmw"
    SRC_URI = """
        Glow_in_the_Dahrk-45886-2-8-2-1584579862.7z
        devtools? ( Mesh_Converter_Scripts_and_Instructions-45886-1-0.7z )
    """
    # TODO: Patches are available for:
    # - Castle Kanthrock
    # - Castle Hestatur
    # - Dark Molag Mar
    # - Windoors
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45886"
    TEXTURE_SIZES = "512 1024"
    # TODO: DATA_OVERRIDES = "distant-lights"
    IUSE = "tr interior-sunrays telvanni-dormers glass devtools"
    INSTALL_DIRS = [
        InstallDir("00 Core", S="Glow_in_the_Dahrk-45886-2-8-2-1584579862"),
        InstallDir(
            "01 Hi Res Window Texture Replacer",
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "02 Interior Sunrays",
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="interior-sunrays",
        ),
        InstallDir(
            "03 Nord Glass Windows",
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="glass !interior-sunrays",
        ),
        InstallDir(
            "03 Nord Glass Windows Interior Sunrays",
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="interior-sunrays",
        ),
        InstallDir(
            "04 Telvanni Dormers on Vvardenfell",
            PLUGINS=[
                File("GITD_Telvanni_Dormers_Interiors.ESP"),
                File("GITD_Telvanni_Dormers_Exterior.ESP"),
                File("GITD_Telvanni_Dormers.ESP"),
            ],
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="telvanni-dormers",
        ),
        InstallDir(
            "05 Raven Rock Glass Windows",
            PLUGINS=[File("GITD_WL_RR_Interiors.esp")],
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="glass !interior-sunrays",
        ),
        InstallDir(
            "05 Raven Rock Glass Windows Interior Sunrays",
            PLUGINS=[File("GITD_WL_RR_Interiors.esp")],
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="glass interior-sunrays",
        ),
        InstallDir(
            "06 Tamriel Rebuilt Patch",
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="tr !interior-sunrays",
        ),
        InstallDir(
            "06 Tamriel Rebuilt Patch Interior Sunrays",
            S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
            REQUIRED_USE="tr interior-sunrays",
        ),
        # TODO: Misc mods are for Castle Kanthrock and Castle Hestatur
        # InstallDir(
        #    "07 Miscellaneous Mods Patch", S="Glow_in_the_Dahrk-45886-2-8-2-1584579862"
        # ),
        # InstallDir(
        #    "07 Miscellaneous Mods Patch Interior Sunrays",
        #    S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
        #    REQUIRED_USE="interior-sunrays",
        # ),
        # InstallDir(
        #    "08 Dark Molag Mar Patch", S="Glow_in_the_Dahrk-45886-2-8-2-1584579862"
        #    REQUIRED_USE="dark-molag-mar !interior-sunrays"
        # ),
        # InstallDir(
        #    "08 Dark Molag Mar Patch Interior Sunrays",
        #    S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
        #    REQUIRED_USE="dark-molag-mar interior-sunrays"
        # ),
        # InstallDir(
        #    "09 Windoors Patch",
        #    PLUGINS=[File("RR_Windoors_Glow_1C.esp")],
        #    S="Glow_in_the_Dahrk-45886-2-8-2-1584579862",
        #    REQUIRED_USE="windoors",
        # ),
        InstallDir(
            ".",
            S="Mesh_Converter_Scripts_and_Instructions-45886-1-0",
            REQUIRED_USE="devtools",
        ),
    ]
