# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Unique Tavern Signs for Tamriel Rebuilt"
    DESC = (
        "Replaces the Imperial tavern signs in Tamriel Rebuilt with new and unique ones"
    )
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46794"
    NEXUS_URL = HOMEPAGE
    KEYWORDS = "openmw"
    LICENSE = "free-derivation"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        landmasses/tamriel-rebuilt
        landmasses/tamriel-data
    """
    TEXTURE_SIZES = "2048"
    COLOURED = "Unique_Tavern_Signs_COLORED_v2-46794-2-1599323795"
    VANILLA = "Unique_Tavern_Signs_VANILLA-ESQUE_v2-46794-2-1599323830"
    SRC_URI = f"""
        !minimal? ( {COLOURED}.7z )
        minimal? ( {VANILLA}.7z )
    """
    IUSE = "minimal"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S=COLOURED,
            PLUGINS=[File("Unique_Tavern_Signs_for_Tamriel_Rebuilt_v2.ESP")],
            REQUIRED_USE="!minimal",
        ),
        InstallDir(
            ".",
            S=VANILLA,
            PLUGINS=[File("Unique_Tavern_Signs_for_Tamriel_Rebuilt_v2.ESP")],
            REQUIRED_USE="minimal",
        ),
    ]
