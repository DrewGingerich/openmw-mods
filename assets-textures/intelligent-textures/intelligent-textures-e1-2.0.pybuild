# Copyright 2019-2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Intelligent Textures"
    DESC = "Replaces almost all vanilla textures with high resolution AI upscales"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/47469"
    NEXUS_URL = HOMEPAGE
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    TEXTURE_SIZES = "2048"
    SRC_URI = """
        Intelligent_Textures-47469-2-0-1671912841.7z
    """
    IUSE = "atlas"
    RDEPEND = "atlas? ( assets-misc/project-atlas )"
    DATA_OVERRIDES = "assets-misc/project-atlas[-atlasgen]"
    TIER = 1
    INSTALL_DIRS = [
        InstallDir("00 Core", S="Intelligent_Textures-47469-2-0-1671912841"),
        InstallDir(
            "01 Atlas Textures",
            REQUIRED_USE="atlas",
            S="Intelligent_Textures-47469-2-0-1671912841",
        ),
    ]
