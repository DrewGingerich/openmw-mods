# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Aesthesia Groundcover"
    DESC = "Grass for Morrowind and Tamriel Rebuilt. Made for every region"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46377"
    KEYWORDS = "openmw"
    LICENSE = "attribution-derivation"
    NEXUS_URL = HOMEPAGE
    TEXTURE_SIZES = "512 2048"
    SRC_URI = """
        Aesthesia_groundcover-46377-1-1549880413.7z
        texture_size_512? (
            Aesthesia_Groundcover_-_vanilla_edition-46377-1-1557670791.7z
        )
        dark? ( Aesthesia_Groundcover_-_darker-46377-1-1557670851.7z )
    """
    RDEPEND = """
        tr? ( landmasses/tamriel-rebuilt[preview?] )
        !!land-flora/ozzy-grass
        !!land-flora/vurts-groundcover[-minimal]
    """
    IUSE = "dark tr preview +bloodmoon"
    INSTALL_DIRS = [
        InstallDir(".", S="Aesthesia_groundcover-46377-1-1549880413"),
        InstallDir(
            "Grass Vanilla",
            GROUNDCOVER=[
                File("Grass Vanilla 1.esp"),
                File("Grass Bloodmoon.esp", REQUIRED_USE="bloodmoon"),
                File("Grass Vanilla 2.esp"),
            ],
            S="Aesthesia_groundcover-46377-1-1549880413",
        ),
        InstallDir(
            "Grass Vanilla regions",
            GROUNDCOVER=[
                # Note rr-ghostgate-fortress has a patched version of the
                # Grass AL.esp plugin
                File("Grass AL.esp"),
                File("Grass Bloodmoon.esp", REQUIRED_USE="bloodmoon"),
                File("Grass BC.esp"),
                File("Grass AC.esp"),
                File("Grass WG.esp"),
                File("Grass GL.esp"),
                File("Grass AI.esp"),
            ],
            S="Aesthesia_groundcover-46377-1-1549880413",
        ),
        InstallDir(
            "Grass TR",
            GROUNDCOVER=[
                File("Grass TR WG.esp"),
                File("Grass TR AI.esp"),
                File("Grass TR GL.esp"),
                File("Grass TR AT.esp"),
                File("Grass TR SV.esp"),
                File("Grass TR AC.esp"),
                File("Grass TR AL.esp"),
                File("Grass TR BC.esp"),
            ],
            S="Aesthesia_groundcover-46377-1-1549880413",
            REQUIRED_USE="tr",
        ),
        InstallDir(
            "Grass TR preview",
            GROUNDCOVER=[
                File("Grass TRp SV.esp"),
                File("Grass TRp AC.esp"),
                File("Grass TRp WG.esp"),
                File("Grass TRp AI.esp"),
                File("Grass TRp AT.esp"),
                File("Grass TRp BC.esp"),
                File("Grass TRp AL.esp"),
                File("Grass TRp GL.esp"),
            ],
            S="Aesthesia_groundcover-46377-1-1549880413",
            REQUIRED_USE="tr preview",
        ),
        InstallDir(
            ".",
            S="Aesthesia_Groundcover_-_vanilla_edition-46377-1-1557670791",
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            ".",
            S="Aesthesia_Groundcover_-_darker-46377-1-1557670851",
            REQUIRED_USE="dark",
        ),
    ]
