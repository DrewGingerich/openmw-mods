# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil

from pybuild.info import P

from common.mw import DOWNLOAD_DIR, MW, File, InstallDir


class Package(MW):
    NAME = "Uvirith's Legacy"
    DESC = "Turns Tel Uvirith into a fitting home for a mage lord and symbol power"
    HOMEPAGE = "https://stuporstar.sarahdimento.com/"
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        tr? ( landmasses/tamriel-rebuilt )
        roht? ( quests-factions/rise-of-house-telvanni )
    """
    TEXTURE_SIZES = "512"
    SRC_URI = f"""
        https://www.dropbox.com/s/sqxbej0nef7bg9e/Uviriths_Legacy_3.53.7z?dl=1 -> {P}.7z
        https://drive.google.com/uc?id=15epmtn1NSOS_veshHW3IgTnoYOV6qzPa&e=download
        -> Uvi_Teleport_fix.esp
        https://modding-openmw.com/files/UL_3.5_OpenMW_1.3_Add-on.7z -> UL_3.5_OpenMW_1.3_Add-on.7z
    """
    IUSE = "roht +chess tr"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            PLUGINS=[File("Uvirith's Legacy_3.53.esp"), File("Uvi_Teleport_fix.esp")],
            S=P,
        ),
        InstallDir(
            "Data Files/Addons",
            BLACKLIST=[
                "UL_3.5_MWSE_Add-on.esp",
                "UL_MWSE_Companions_v2.esp",
                "UL_Upgrader_MWSE.esp",
            ],
            PLUGINS=[
                File("UL_Chess_Add-on.esp", REQUIRED_USE="chess"),
                File("UL_3.5_RoHT_1.52_Add-on.esp", REQUIRED_USE="roht"),
                File("UL_3.5_TR_16.12_Add-on.esp", REQUIRED_USE="tr"),
                # File("UL_AshArmor_Plangkye.esp"),
                # File("UL_AshArmor_DivineDomina.esp"),
                # File("UL_WeaponRotate_Addon.esp"),
                # The Lighting Mod
                # File("UL_TLM_Ambiance.esp"),
                # File("UL_BookJackets_Add-on.esp"),
                # Children of Morrowind
                # File("UL_CoM_Add-on.esp"),
                # File("Stinkers 2.0 to 3.0 Upgrader.esp"),
            ],
            S=P,
        ),
        InstallDir(
            ".",
            PLUGINS=[File("UL_3.5_OpenMW_1.3_Add-on.omwaddon")],
            S="UL_3.5_OpenMW_1.3_Add-on",
        ),
    ]

    def src_unpack(self):
        for source in self.A:
            if source.name == "Uvi_Teleport_fix.esp":
                shutil.copy(source.path, f"{P}/Data Files")
            else:
                self.unpack([source])

    def pkg_nofetch(self):
        super().pkg_nofetch()
        print("Please download the following file from the url at the bottom")
        print(
            "before continuing and move them to one of the following download directories:"
        )
        print(f"  {DOWNLOAD_DIR}")
        print("  " + os.path.expanduser("~/Downloads"))
        print()
        print("  UL_3.5_OpenMW_1.3_Add-on.7z")
        print()
        print(
            "   https://mega.nz/#!FEwSzSoS!q4EPrN_0aaxWE05TTCdNYhHd1fzTewi1pzcCwjR3glg"
        )
